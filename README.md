Game for the Game Development module at the University of Derby.

TEAM MEMBERS

Programmers:

- Adam
- Ben
- Brandon
- Jordan
- Pete

Artists:

- Eva
- James
- Joe
- Marcus
- Mathew
- Rebecca